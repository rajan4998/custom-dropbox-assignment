<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

    //web app routes
    Route::get('/user-upload','UserController@loadUploadView');
    Route::post('/upload','UserController@upload');
    Route::get('/download/{id}','UserController@downloadFile');
    Route::get('/','UserController@index');

    Route::get('/test',function(){
        return view('test');
    });

    Route::get('/test1','UserController@test');

    //api routes
    Route::post('/get_user_data','UserController@getUserContent');
    Route::post('/create_folder','UserController@createUserFolder');
});
