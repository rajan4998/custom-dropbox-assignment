@extends('layouts.app')

@section('content')
<link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">

<div ng-app="Dropbox" ng-controller="DropboxCtrl" class="container">
    <div class="row">
        <p>Welcome {{ Auth::user()->name }} to the User Dashboard</p>
        <a class="btn btn-sm btn-primary add-btn" href="/" data-toggle="modal" data-target="#myModal">Add</a>
    </div>
    <div ng-init="load()" class="row">
        <div class="col-md-12">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Created On</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>This is my Name.txt</td>
                        <td>Otto</td>
                        <td>
                            <a href="/" class="btn-sm btn-primary">Edit</a>
                            <a href="/" class="btn-sm btn-danger">Delete</a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">


                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">


                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#create_folder">Create a Folder</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#create_file">Create a File</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="create_folder" class="container tab-pane active"><br>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-3 col-form-label">Folder Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control folder_name" placeholder="Your Folder Name Here">
                                </div>
                                <div class="col-sm-2">
                                    <a href="#" class="btn btn-primary btn-md float-right" ng-click="addFolder()">Create</a>
                                </div>
                            </div>
                        </div>
                        <div id="create_file" class="container tab-pane fade"><br>
                            <form class="form-horizontal" method="post" action="{{ url('upload') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Choose a file</label>
                                    <div class="col-sm-7">
                                        <input id="files" name="files" class="input-file" type="file">
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="sbumit" class="btn btn-primary btn-md float-right">Upload</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/angular-1.6.9.min.js') }}"></script>
<script>
var app = angular.module('Dropbox',[]);
app.controller('DropboxCtrl',function($scope,$http){


    $scope.load = function(){
        get_user_data();
    }

    $scope.addFolder = function(){
        var folder_name = angular.element(document.querySelector('.folder_name')).val();
        $http({
            method : "POST",
            url : "{{ url('/create_folder') }}",
            data : {
                'folder_name' : folder_name
            },
        })
        .then(function(response) {
            console.log(response);
            // if(response.data.code===200){
            //     var post = response.data.post;
            //     for(var i=0;i < $scope.posts.length;i++){
            //         if(post.id==$scope.posts[i].id){
            //             $scope.posts[i].title= post.title;
            //             $scope.posts[i].short_description = post.short_description;
            //             $scope.posts[i].description = post.description;
            //             break;
            //         }
            //     }
            // }
        });

    }

    function get_user_data(){
        $http({
            method : "POST",
            url : "{{ url('/get_user_data') }}",
            data : {
                "directory_location" : 'root'
            },
        });
    }
});

// });
</script>

@endsection
