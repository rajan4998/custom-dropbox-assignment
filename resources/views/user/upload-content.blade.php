@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4 offset-md-3">
                <form class="form-horizontal" method="post" action="{{ url('upload') }}" enctype="multipart/form-data">
                    <fieldset>
                        {{ csrf_field() }}

                        <!-- Form Name -->
                        <legend>Upload</legend>

                        <!-- File Button -->
                        <div class="form-group">
                            <!-- <label class="col-md-4 control-label" for="filebutton"></label> -->
                            <div class="col-md-4">
                                <input id="files" name="files" class="input-file" type="file">
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="singlebutton"></label>
                            <div class="col-md-12">
                                <a class="btn btn-primary" href="{{ url('/') }}">Back</a>
                                <button id="singlebutton" name="singlebutton" class="btn btn-primary">Submit</button>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection('content')
