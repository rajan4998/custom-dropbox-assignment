@extends('layouts.app')

@section('content')

<div class="container">
  <h2>User Dashboard <a href="{{ url('user-upload') }}" class="btn btn-primary">Add</a></h2>
  <p>User uploaded content :</p>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>File Name</th>
        <th>Uploaded On</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
        @foreach($userData as $index => $user_data)
      <tr>
        <td>{{ $user_data->file_name }}</td>
        <td>{{ $user_data->updated_time }}</td>
        <td>
            <a class="btn btn-primary btn-xs">Edit</a>
            <a href="{{ url("download/$user_data->id") }}" class="btn btn-primary btn-xs">Download</a>
            <a class="btn btn-danger btn-xs">Delete</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

@endsection('content')
