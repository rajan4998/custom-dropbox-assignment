<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Folders extends Model
{
    protected $table = "folders";

    protected $fillable = [
        'user_id',
        'name',
        'parent',
        'is_root'
    ];

    public static function create_user_folder(Request $r){
        $folder_name = $r->post('folder_name');
        $parent_id = ($r->post('parent_id')!=null) ? $r->post('parent_id') : 0;

        $folder_count = Folders::where('user_id',$r->user()->id)
        ->where('is_root',1)
        ->count();

        if($folder_count == 0){
            $root_folder_name = 'user_'.$r->user()->id;
            Storage::makeDirectory($root_folder_name);
            $full_path = public_path().'/uploads/'.$root_folder_name;
            chmod($full_path, 0777);
            Folders::create_folder($r->user()->id,$root_folder_name,0,1);
        }

        Folders::create_folder($r->user()->id,$folder_name,$parent_id,0);
    }

    public static function create_folder($user_id,$folder_name,$parent_id,$is_root){
        Folders::create([
            'user_id' => $user_id,
            'name' => $folder_name,
            'parent' => $parent_id,
            'is_root' => $is_root
        ]);
    }

    public static function getAllFiles(Request $r){
        $all_folders = Folders::where('user_id',$r->user()->id)->get();
        dd($all_folders);
    }
}
