<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected $table = "files";

    protected $fillable = [
        'folder_id',
        'file_name',
        'extension'
    ];

    public static function verifyFileSize($file_size){

        $max_file_size = $_ENV['MAX_FILE_SIZE'];

        if($file_size <= $max_file_size){
            return response([
                'status' => 1
            ]);
        }
        else{
            return response()->json([
                'status' => 0
            ]);
        }
    }
}
