<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Files;
use App\Models\UserContent;
use App\Models\Folders;
use File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index(Request $r){

        $user_data = UserContent::select('id','file_name','updated_at')->where('user_id',$r->user()->id)->get()
        ->map(function($i){
            $i['updated_time'] = \Carbon\Carbon::parse($i['updated_at'])->format('d M, Y');
            unset($i['updated_at']);
            return $i;
        });

        return view('user.user-dashboard')->withuserData($user_data);
    }

    public function loadUploadView(Request $r){
        return view('user.upload-content');
    }

    public function upload(Request $r){

        // dd($r->hasFile('files'));
        if($r->hasFile('files')){

            $file = $r->file('files');
            // dd($file->getPathName());
            $file_name = $file->getClientOriginalName();
            $file_size = $file->getSize();

            $file_size_status = Files::verifyFileSize($file_size);

            if($file_size_status->original['status']!=1){
                abort(400);
            }

            $extension_array = explode('.',$file_name);
            $extension = end($extension_array);
            $extension = base64_encode($extension);

            //Move Uploaded File
            $destinationPath = public_path() . "/uploads/user_".$r->user()->id.'/';

            if(!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true, true);
            }

            $file_contents = File::get($file->getPathName());

            $encrypted_file_contents = encrypt($file_contents);

            $file_path = $r->user()->id.'_'.time();

            File::put($destinationPath.$file_path, $encrypted_file_contents);

            $full_file_path = $destinationPath.$r->user()->id.'_'.time();

            UserContent::create([
                'file_name' => $file_name,
                'file_path' => $file_path,
                'user_id' => $r->user()->id,
                'extension' => $extension
            ]);

            $message = "File uploaded successfully";
        }else{
            $message = "File upload failed";
        }

        return redirect()->back()->withMessage([$message]);
    }

    public function downloadFile($file_id,Request $r){
        $file_data = UserContent::findOrFail($file_id);

        if($file_data->user_id!=$r->user()->id){
            abort(403);
        }

        $directory_path = public_path().'/uploads/user_'.$r->user()->id.'/';
        $file_path = $directory_path.$file_data->file_path;

        if(!File::exists($file_path)){
            abort(404);
        }

        $decoded_extension = $file_data->extension;
        $temp_file = $directory_path.$file_data->file_name;
        $file_contents = File::get($file_path);
        $decrypted_contents = decrypt($file_contents);

        File::put($temp_file, $decrypted_contents);

        return response()->download($temp_file)->deleteFileAfterSend();
    }

    public function getUserContent(Request $r){

        $directory      = '/user_'.$r->user()->id;
        $user_content   = [];
        $folders        = [];
        $files          = [];

        $folders    = Storage::directories($directory); //directories
        $files      = Storage::files($directory); //files

        $user_content['folders']    = $folders;
        $user_content['files']      = $files;

        return response()->json($user_content);
    }

    public function createUserFolder(Request $r){

        if($r->post('folder_name')=="" || $r->post('folder_name')==null){
            return response()->json([
                'status' => 0,
                'message' => 'Invalid Parameters'
            ]);
        }
        Folders::create_user_folder($r);

        return response()->json([
            'status' => 1,
            'message' => 'success'
        ]);
    }

    public function test(){
        // getAllFiles
    }
}
